<?php
session_start();
include_once "sesi.php";
$modul=(isset($_GET['m']))?$_GET['m']:"awal";
$jawal="Admin Gudang Barang";
switch($modul){
	case 'awal': 
		default: 
		$aktif="Beranda"; 
		$judul="Beranda $jawal"; 
		include "awal.php"; 
	break;

	case 'admin': 
		$aktif="Admin"; 
		$judul="Modul $jawal"; 
		include "modul/administrator/index.php"; 
	break;

	case 'supplier':
		$aktif="Supplier";
		$judul="Modul Supplier $jawal";
		include "modul/supplier/index.php";
	break;
	case 'kategori':
		$aktif="Kategori";
		$judul="Modul Kategori $jawal";
		include "modul/kategori/index.php";
	break;

	case 'barang':
		$aktif="Barang";
		$judul="Modul Barang $jawal";
		include "modul/barang/index.php";
	break;

	case 'barang_masuk':
		$aktif="Barang_masuk";
		$judul="Modul Barang Masuk $jawal";
		include "modul/barang_masuk/index.php";
	break;

	case 'barang_keluar':
		$aktif="Barang_keluar";
		$judul="Modul Barang Keluar $jawal";
		include "modul/barang_keluar/index.php";
	break;
	
	case 'laporan_masuk_supplier':
		$aktif="laporan_masuk_supplier";
		$judul="Modul laporan masuk supplier $jawal";
		include "modul/laporan_masuk_supplier/index.php";
	break;
	case 'laporan_masuk':
		$aktif="laporan_masuk";
		$judul="Modul laporan masuk $jawal";
		include "modul/laporan_masuk/index.php";
	break;
	case 'laporan_keluar':
		$aktif="laporan_keluar";
		$judul="Modul laporan kelar $jawal";
		include "modul/laporan_keluar/index.php";
	break;

	case 'kandidat':
		$aktif="Kandidat";
		$judul="Modul Kandidat $jawal";
		include "modul/booking/index.php";
	break;
}

?>
