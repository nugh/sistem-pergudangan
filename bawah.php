		</div>
		<!-- /.content-wrapper - adanya di file content -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>Sistem Informasi Pergudangan Cafe Konoiku<a>
				<div style="color: black;
					padding: 1px 1px 1px 1px;
					float: right;
					font-size: 16px;">Date :
                    <?php date_default_timezone_set('Asia/Jakarta');
					echo date('d-m-Y'); ?> &nbsp; &nbsp;</div>
      </footer>
    </div><!-- ./wrapper - adanya di atas.php -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
	<!-- page script -->
	<script>
	  $(function(){
	  	$('#example1').DataTable()
		$('#pilkasis1').DataTable();
		$('#pilkasis2').DataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
		$('#pilkasis3').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": false,
		  "info": true,
		  "autoWidth": false
		});
		//Date picker
    	$('#datepicker1').datepicker({
      	autoclose: true
    		})
	  });
	</script>
  </body>
</html>
