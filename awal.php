<?php include "atas.php"; ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <marquee behavior="" direction="" style="font-size: 19px; color: maroon;">Selamat Datang Admin Gudang Cafe Konoiku</marquee>
    </section>
		<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="pilkasis1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Stok</th>
                  <th>Kategori Tempat</th>                  
                </tr>
                </thead>
                <tbody>
                
<?php
include "lib/config.php";
$sql="SELECT *,kategori.nama_kategori FROM barang 
JOIN kategori ON kategori.id_kategori = barang.id_kategori
ORDER BY kode_barang";
$query=mysqli_query($koneksi,$sql);
	$no=1;
	while($r=mysqli_fetch_assoc($query)){
	  echo "<tr>";
		echo "<td>$no</td>";
		echo "<td>".$r['kode_barang']."</a></td>";
		echo "<td>".$r['nama_barang']."</td>";
    echo "<td>".$r['stok']."</td>";   
    echo "<td>".$r['nama_kategori']."</td>";    
	  echo "</tr>";
		$no++;
	}
?>
                </tbody>  
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
</div>

    
<?php include "bawah.php"; ?>
