<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SI Gudang </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  </head>
  <body lang="id-ID" class="skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="." class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>O</b>sis</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs"><?php echo $_SESSION['nama']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="dist/img/logo.jpg" class="img-circle" alt="User Image">
                    <p>
                       <?php echo $_SESSION['nama']; ?> 
                      <small><?php echo $_SESSION['jabatan']; ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!-- <div class="pull-left">
                      <a href="?m=admin&s=profil" class="btn btn-info">Profile</a>
                    </div> -->
                    <div class="pull-right">
                      <a href="logout.php" class="btn btn-info">Logout</a>
                    </div>
                  </li>
                </ul>
              </li>
            
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/logo.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= $_SESSION['nama']?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>



      <!-- sidebar menu: : style can be found in sidebar.less -->

<ul class="sidebar-menu" data-widget="tree">
        <div style="height: 30px;"></div>

<!-- Master -->
<li class="treeview <?php if($aktif=='Barang') echo 'active';if($aktif=='Supplier') echo 'active';if($aktif=='Kategori') echo 'active';?>">
  <a href="#"><i class="fa fa-navicon"></i> <span>Akses</span>
<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
  <ul class="treeview-menu">

  <li class="<?php if($aktif=='Barang') echo 'active';?>"><a href="?m=barang"><i class="fa fa-building-o"></i> Barang</a></li>  
  <li class="<?php if($aktif=='Supplier') echo 'active';?>"><a href="?m=supplier"><i class="fa fa-user-o"></i> Supplier</a></li>
  <li class="<?php if($aktif=='Kategori') echo 'active';?>"><a href="?m=kategori"><i class="fa fa-user-o"></i> Kategori Tempat</a></li>
  </ul>
</li>
<!-- /Master-->
  
  <li class="<?php if($aktif=='Barang_masuk') echo 'active';?>"><a href="?m=barang_masuk"><i class="fa fa-navicon"></i> <span>Barang Masuk</span></a></li>
  <li class="<?php if($aktif=='Barang_keluar') echo 'active';?>"><a href="?m=barang_keluar"><i class="fa fa-navicon"></i> <span>Barang Keluar</span></a></li>

<!-- Laporan -->
<li class="treeview <?php if ($aktif=='laporan_masuk_supplier') echo 'active';if($aktif=='laporan_masuk') echo 'active';if($aktif=='laporan_keluar') echo 'active';?>">
  <a href="#"><i class="fa fa-navicon"></i> <span>Laporan</span>
<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
  <ul class="treeview-menu">
	
	<li class="<?php if($aktif=='laporan_masuk_supplier') echo 'active';?>"><a href="?m=laporan_masuk_supplier"><i class="fa fa-building-o"></i> Laporan Supplier</a></li>  
	<li class="<?php if($aktif=='laporan_masuk') echo 'active';?>"><a href="?m=laporan_masuk"><i class="fa fa-building-o"></i> Barang Masuk</a></li>  
	<li class="<?php if($aktif=='laporan_keluar') echo 'active';?>"><a href="?m=laporan_keluar"><i class="fa fa-building-o"></i> Barang Keluar</a></li>
  </ul>
</li>
<!-- /Laporan-->

        
</ul> <!-- end of sidebar-menu -->
    </section>
    <!-- /.sidebar -->




  </aside>
