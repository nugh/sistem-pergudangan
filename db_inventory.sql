-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2020 at 09:30 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `stok` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `stok`, `id_kategori`) VALUES
('B001', 'Kopi Arabica', 365, 1),
('B003', 'Mie Instan Indomie', 100, 2),
('B004', 'Sosis', 105, 3),
('B005', 'Teh', 100, 2),
('B006', 'Susu Diamond', 50, 1),
('B007', 'Nugget', 40, 4),
('B008', 'Melon', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barang_keluar` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barang_keluar`, `kode_barang`, `qty`, `tanggal`) VALUES
(18, 'B001', 5, '2019-12-14'),
(19, 'B004', 5, '2019-12-16'),
(21, 'B001', 50, '2019-12-19'),
(22, 'B001', 5, '2020-03-09');

--
-- Triggers `barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `update_stok_keluar` AFTER INSERT ON `barang_keluar` FOR EACH ROW BEGIN
	UPDATE barang SET stok=stok - NEW.qty
	WHERE kode_barang = NEW.kode_barang;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barang_masuk` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `total_harga` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barang_masuk`, `kode_barang`, `qty`, `tanggal`, `id_supplier`, `harga`, `total_harga`) VALUES
(27, 'B001', 10, '2019-12-14', 3, '100000', '1000000'),
(28, 'B001', 10, '2019-12-14', 3, '100000', '1000000'),
(29, 'B001', 5, '2019-12-14', 3, '100000', '500000'),
(30, 'B004', 50, '2019-12-16', 3, '3000', '150000'),
(31, 'B003', 20, '2019-12-17', 3, '20000', '400000'),
(33, 'B001', 1, '2019-12-18', 3, '20000', '20000'),
(35, 'B001', 77, '2019-12-18', 3, '80000', '6160000'),
(37, 'B006', 50, '2019-12-19', 8, '20000', '1000000'),
(38, 'B001', 90, '2019-12-19', 3, '20000', '1800000'),
(39, 'B007', 40, '2019-12-19', 3, '50000', '1600000'),
(42, 'B004', 40, '2019-12-19', 7, '5000', '200000'),
(43, 'B001', 40, '2019-12-19', 11, '37000', '180000'),
(46, 'B001', 11, '2019-12-19', 4, '115000', '115500'),
(47, 'B004', 20, '2019-12-19', 4, '500', '10000'),
(48, 'B001', 50, '2019-12-19', 3, '6000', '300000'),
(49, 'B001', 40, '2020-03-09', 3, '10000', '400000'),
(50, 'B001', 11, '2020-03-12', 3, '100000', '1100000');

--
-- Triggers `barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `update_stok_masuk` AFTER INSERT ON `barang_masuk` FOR EACH ROW BEGIN
	UPDATE barang SET stok=stok + NEW.qty
	WHERE kode_barang = NEW.kode_barang;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Lemari A'),
(2, 'Lemari B'),
(3, 'Lemari ES A'),
(4, 'Lemari ES B'),
(5, 'Lemari Buah'),
(6, 'Laci');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jabatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `password`, `nama`, `jabatan`) VALUES
('', 'd41d8cd98f00b204e9800998ecf8427e', '', ' '),
('11', '6512bd43d9caa6e02c990b0a82652dca', '11', ' '),
('123456789101112131415', 'd92fe77e057d3fc33deca67c86bc2b97', 'qwertyuioplkjhgfdsa', ' '),
('a a a', '0dea60f9dcec872359b63a126bff3e8d', 'a a a ', ' '),
('admin', '21232f297a57a5a743894a0e4a801fc3', 'ward', 'admin'),
('admin2', '21232f297a57a5a743894a0e4a801fc3', 'admin2', ' '),
('anu', '89a4b5bd7d02ad1e342c960e6a98365c', 'anu', ' '),
('anu?', '9f4589d3ea29119f6e8394133d25b63d', 'sdaasd', ' '),
('anu[]', '25ce62bef04fec099f39ef4469e7538d', 'anu[]', ' '),
('dea', 'a01610228fe998f515a72dd730294d87', 'dea', ' '),
('dsada123', 'c235819998915fef7f76cabfc7dcb069', 'dsada', ' '),
('wahyu', 'pass', 'nama', '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(200) NOT NULL,
  `no_telp` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `no_telp`, `alamat`) VALUES
(3, 'Joko', '0898989', 'Sda'),
(4, 'Supri', '0987990', 'Sby'),
(7, 'DinoMall', '0899', 'Sidoarjo'),
(11, 'PT. Kapal Api', 'a0966785', 'SDA'),
(13, 'AAA', '09776666688', 'anananna');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barang_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barang_masuk`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
