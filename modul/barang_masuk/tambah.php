<?php include "atas.php"; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Gudang        
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Form Barang Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <!--Mulai buat form-->
			 <form action="?m=barang_masuk&s=simpan" method="post" enctype="multipart/form-data">
              <table class="table table-bordered table-hover table-striped">
                <tbody>
           <tr><td>
                <label>Pilih Tanggal</label></td>
                <td>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input required  value="<?php echo date("m/d/Y");?>" type="text" name="tanggal" class="form-control pull-right" id="datepicker1">
                </div>
                <!-- /.input group -->
              </td>
           </tr>       
          <tr>
            <td width=150>Barang</td>
            <td>
              <select name="kode_barang" class="form-control">
                <?php
                include "lib/config.php";
                $sql    = "SELECT * FROM barang ORDER BY kode_barang";
                $query  = mysqli_query($koneksi,$sql);
                while ($r=mysqli_fetch_assoc($query)) {
                  echo "<option value='".$r['kode_barang']."'>".$r['nama_barang']."</option>";
                }
                ?>
              </select>
            </td>
          </tr>
          <tr>
            <td width=150>Harga</td>

            <td><input id="harga_currency" onkeyup="number_currency_(this);" class="form-control" type="number" placeholder="harga"/>
              <input id="harga" class="form-control" type="hidden" name="harga" placeholder="harga"/>
            </td>
          </tr>
          <tr>
            <td width=150>Jumlah</td>
            <td><input id="jumlah" onchange="total()" class="form-control" type="number" min="1" name="qty" placeholder="jumlah barang"/></td>
          </tr>
          <tr>
            <td width=150>Supplier</td>
            <td>
              <select name="supplier" class="form-control">
                <?php
                $sql    = "SELECT * FROM supplier ORDER BY id_supplier";
                $query  = mysqli_query($koneksi,$sql);
                while ($r=mysqli_fetch_assoc($query)) {
                  echo "<option value='".$r['id_supplier']."'>".$r['nama_supplier']."</option>";
                }
                ?>
              </select>
            </td>
          </tr>
          <tr>
            <td width=150>Total Harga</td>
            <td><input id="tot" class="form-control" type="text" name="tot_harga" readonly/>
            </td>
          </tr>          
					<tr>
						<td colspan=2>
						<input type="submit" name="simpan" value="Save" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;
						<input type="reset" name="reset" value="Reset" class="btn btn-large btn-warning" />&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
                </tbody>
              </table>
			 </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
<script type="text/javascript">
  

  function total(){

    
    var harga = parseInt(document.getElementById('harga').value);
    var qty = parseInt(document.getElementById('jumlah').value);
    var total = harga * qty ;

    document.getElementById('tot').value = total;
  }


  function number_currency_(elem){
  var elem_id = '#'+elem.id;
  var elem_val   = $(elem_id).val();
  var elem_no_cur = elem_id.replace(/_currency/g,'');

  var str = elem_val.toString(), parts = false, output = [], i = 1, formatted = null;

  parts = str.split(".");
  var gabung = '';
  for (var i = 0; i < parts.length; i++) {
   var gabung = gabung+parts[i];
  }

  str = gabung.split("").reverse();
  var i = 1;
  for(var j = 0, len = gabung.length; j < len; j++) {
   if(str[j] != ".") {
     output.push(str[j]);
     if(i%3 == 0 && j < (len - 1)) {
       output.push(".");
     }
     i++;
   }
  }

  formatted = output.reverse().join("");
  $(elem_id).val(formatted);
  $(elem_no_cur).val(gabung);
}

</script>