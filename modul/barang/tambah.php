<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Inventory Gudang
        
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Form Tambah Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <!--Mulai buat form-->
			 <form action="?m=barang&s=simpan" method="post" enctype="multipart/form-data">
              <table class="table table-bordered table-hover table-striped">
                <tbody>					
          <tr>
            <td width=174>Barang</td>
            <td><input class="form-control" type="text" name="nama_barang" placeholder="Nama Barang"/></td>
          </tr>
          <tr>
            <input class="form-control" type="hidden" name="stok" placeholder="stok" value="0" />
          </tr>
          <tr>
            <td width=150>Kategori Tempat</td>
            <td>
              <select name="id_kategori" class="form-control">
                <?php
                include "lib/config.php";
                $sql    = "SELECT * FROM kategori ORDER BY id_kategori";
                $query  = mysqli_query($koneksi,$sql);
                while ($r=mysqli_fetch_assoc($query)) {
                  echo "<option value='".$r['id_kategori']."'>".$r['nama_kategori']."</option>";
                }
                ?>
              </select>
            </td>
          </tr>
					<tr>
						<td colspan=3>
						<input type="submit" name="simpan" value="Save" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;
						<input type="reset" name="reset" value="Reset" class="btn btn-large btn-warning" />&nbsp;&nbsp;&nbsp;
						<a href="?m=barang" class="btn btn-large btn-danger"><i class="fa fa-times"></i> List</a></td>
					</tr>
                </tbody>
              </table>
			 </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
