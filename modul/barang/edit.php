<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Inventory Gudang
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Form Edit Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<?php
$id=$_GET['id'];
include "lib/config.php";
$sql="SELECT * FROM barang WHERE kode_barang = '$id'";
$query=mysqli_query($koneksi,$sql);
$r=mysqli_fetch_assoc($query);
?>
			 <!--Mulai buat form-->
			 <form action="?m=barang&s=update" method="post" enctype="multipart/form-data">
              <table id="pilkasis1" class="table table-bordered table-hover table-striped">
                <tbody>
					<input type="hidden" name="id" value="<?php echo$r['kode_barang'];?>" />
					<tr>
						<td width=150>Nama Barang</td>
						<td><input class="form-control" type="text" name="nama_barang" value="<?php echo$r['nama_barang'];?>" required /></td>
					</tr>
          <tr>
            <td width=150>Kategori Tempat</td>
            <td>
              <select name="id_kategori" class="form-control">
            <?php
            $sql1    = "SELECT * FROM kategori ORDER BY id_kategori";
            $query1  = mysqli_query($koneksi,$sql1);
            while ($r1=mysqli_fetch_assoc($query1)){ ?>
              <option <?php if($r['id_kategori'] == $r1['id_kategori']){ ?> selected="selected"<?php } ?> value="<?= $r1['id_kategori'] ?>"><?= $r1['nama_kategori']?></option>
                                             <?php } ?>
              </select>
            </td>
          </tr>          
					<tr>
						<td colspan=3>
						<input type="submit" name="simpan" value="Save" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;
						<input type="reset" name="reset" value="Reset" class="btn btn-large btn-warning" />&nbsp;&nbsp;&nbsp;
						<a href="?m=barang" class="btn btn-large btn-danger"><i class="fa fa-times"></i> List</a></td>
					</tr>
                </tbody>
              </table>
			 </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
