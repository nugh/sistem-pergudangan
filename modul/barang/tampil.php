<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator Gudang Cafe Konoiku
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
			  <a href="?m=barang&s=tambah" class="btn bg-navy"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Barang</a>
              <h3 class="box-title">Daftar Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="pilkasis1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Stok</th>
                  <th>Kategori Tempat</th>                  
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                
<?php
include "lib/config.php";
$sql="SELECT *,kategori.nama_kategori FROM barang 
JOIN kategori ON kategori.id_kategori = barang.id_kategori
ORDER BY kode_barang";
$query=mysqli_query($koneksi,$sql);
	$no=1;
	while($r=mysqli_fetch_assoc($query)){
	  echo "<tr>";
		echo "<td>$no</td>";
		echo "<td>".$r['kode_barang']."</a></td>";
		echo "<td>".$r['nama_barang']."</td>";
    echo "<td>".$r['stok']."</td>";   
    echo "<td>".$r['nama_kategori']."</td>";    
		echo '<td width=60><a href="index.php?m=barang&s=edit&id='.$r['kode_barang'].'"><i class="fa fa-edit"></i></a> | <a href="index.php?m=barang&s=hapus&id='.$r['kode_barang'].'" onclick="return confirm(\'Yakin Akan dihapus?\')"><i class="fa fa-remove"></i></a></td>';
	  echo "</tr>";
		$no++;
	}
?>
                </tbody>  
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
