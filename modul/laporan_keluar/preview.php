<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator Inventory Barang
      </h1>
    </section>
    <!-- Main content -->
  <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Lihat Daftar Barang Keluar</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form action="?m=laporan_keluar&s=tampilkan" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-4">
            <div class="form-group">
                <label>Pilih Tanggal</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <?php
                  include 'lib/config.php';
                  $tanggal_ = $_GET['tanggal'];
                  $tanggal = format_back_date($tanggal_);
                  ?>
                  <input required  value="<?=$tanggal_?>" type="text" name="tanggal" class="form-control pull-right" id="datepicker1">
                </div>
                <!-- /.input group -->
              </div>
            </div>
            <div class="col-md-4">
              <input style="margin-top: 25px; " type="submit" name="simpan" value="Tampilkan" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;    
            </div>
          </div>  
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
<div class="box">
        <div class="box-body">
          <table id="pilkasis1" class="table table-bordered table-hover table-striped">
            <a href="index.php?m=laporan_keluar&s=export&tanggal=<?=$tanggal?>" class="btn btn-small btn-primary" style="margin-bottom: 10px;margin-left: 90%;">Export Excel</a>

                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode barang</th>
                  <th>Nama Barang</th>
                  <th>Qty</th>                
                  <th>Tanggal</th>                  
                </tr>
                </thead>
                <tbody>
                
<?php
$sql="SELECT *,nama_barang FROM barang_keluar JOIN barang ON barang.kode_barang = barang_keluar.kode_barang where tanggal = '$tanggal'";
$query=mysqli_query($koneksi,$sql);
  $no=1;
  while($r=mysqli_fetch_assoc($query)){
    echo "<tr>";
    echo "<td>$no</td>";
    echo "<td>".$r['kode_barang']."</a></td>";
    echo "<td>".$r['nama_barang']."</td>";  
    echo "<td>".$r['qty']."</td>";  
    echo "<td>".$r['tanggal']."</td>";  
    echo "</tr>";
    $no++;
  }
?>
                </tbody>  
              </table>
          
        
        </div>  
        </div>


    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>