<?php
$tanggal = $_GET['tanggal'];

include 'lib/fungsi.php';
include 'lib/config.php';

header("Content-Type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporanBarangKeluar.xls");

echo "<table border='1'>";
echo "<tr>";
echo "<th>No</th>";
echo "<th>Kode Barang</th>";
echo "<th>Nama Barang</th>";
echo "<th>Qty</th>";
echo "<th>Tanggal</th>";
echo "</tr>";
$sql="SELECT *,nama_barang FROM barang_keluar JOIN barang ON barang.kode_barang = barang_keluar.kode_barang where tanggal = '$tanggal'";
$query=mysqli_query($koneksi,$sql);
$no = 1;
while ($d= mysqli_fetch_assoc($query)) {
	
	echo '

		<tr>
			<td>'.$no.'</td>
			<td>'.$d['kode_barang'].'</td>
			<td>'.$d['nama_barang'].'</td>
			<td>'.$d['qty'].'</td>
			<td>'.$d['tanggal'].'</td>			
		</tr>

	';
	$no++;
}
echo "</table>";
?>