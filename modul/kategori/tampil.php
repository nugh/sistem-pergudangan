<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Inventory gudang
      </h1>
    </section>
    <!-- Main content -->
  <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
        <a href="?m=kategori&s=tambah" class="btn bg-navy">
          <i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Kategori</a>
              <br><br><h3 class="box-title">Daftar Kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<?php
include "lib/config.php";
$sql="SELECT * FROM kategori ORDER BY id_kategori";
$query=mysqli_query($koneksi,$sql);
    echo'<table id="pilkasis1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
				          <th>No</th>
                  <th>Nama kategori</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
        ';        
  $no=1;
  while($r=mysqli_fetch_assoc($query)){
    echo "<tr>";
    echo "<td>$no</td>";
    echo "<td>".$r['nama_kategori']."</td>";
    echo '<td width=60><a href="index.php?m=kategori&s=edit&id='.$r['id_kategori'].'"><i class="fa fa-edit"></i></a> | <a href="index.php?m=kategori&s=hapus&id='.$r['id_kategori'].'" onclick="return confirm(\'Yakin Akan dihapus?\')"><i class="fa fa-remove"></i></a></td>';
    echo "</tr>";
    $no++;
  }
?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>