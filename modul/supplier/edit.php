<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Inventory Gudang
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Form Edit Supplier</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<?php
$id=$_GET['id'];
include "lib/config.php";
$sql="SELECT * FROM supplier WHERE id_supplier ='$id'";
$query=mysqli_query($koneksi,$sql);
$r=mysqli_fetch_assoc($query);
?>
			 <!--Mulai buat form-->
			 <form action="?m=supplier&s=update" method="post" enctype="multipart/form-data">
              <table id="pilkasis1" class="table table-bordered table-hover table-striped">
                <tbody>
					<input type="hidden" name="id" value="<?php echo$r['id_supplier'];?>" />
					<tr>
						<td width=150>Nama Supplier</td>
						<td><input class="form-control" type="text" name="nama_supplier" placeholder="nama" size="7px" maxlength="7px" value="<?php echo$r['nama_supplier'];?>"/></td>
					</tr>
					<tr>
            <tr>
            <td width=150>No Telp</td>
            <td><input class="form-control" type="number" name="telp" placeholder="no telp" value="<?php echo$r['no_telp'];?>"/></td>
          </tr>
          <tr>
            <td width=150>Alamat</td>
            <td><input class="form-control" type="text" name="alamat" placeholder="alamat" size="7px" maxlength="7px" value="<?php echo$r['alamat'];?>"/></td>
          </tr>
						<td colspan=3>
						<input type="submit" name="simpan" value="Save" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;
						<input type="reset" name="reset" value="Reset" class="btn btn-large btn-warning" />&nbsp;&nbsp;&nbsp;
						<a href="?m=supplier" class="btn btn-large btn-danger"><i class="fa fa-times"></i> List</a></td>
					</tr>
                </tbody>
              </table>
			 </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
