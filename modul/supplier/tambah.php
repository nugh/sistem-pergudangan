<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Inventory Gudang
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Form Tambah Supplier</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <!--Mulai buat form-->
			 <form action="?m=supplier&s=simpan" method="post" enctype="multipart/form-data">
              <table class="table table-bordered table-hover table-striped">
                <tbody>
					<tr>
						<td width=174>Nama Supplier</td>
						<td><input class="form-control" type="text" name="nama_supplier" placeholder="nama" /></td>
					</tr>
          <tr>
            <td width=174>No Telp</td>
            <td><input class="form-control" type="number" name="no_telp" placeholder="telp" min="11" max="13" /></td>
          </tr>
          <tr>
            <td width=174>Alamat</td>
            <td><input class="form-control" type="text" name="alamat" placeholder="alamat" /></td>
          </tr>
					<tr>
						<td colspan=3>
						<input type="submit" name="simpan" value="Save" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;
						<input type="reset" name="reset" value="Reset" class="btn btn-large btn-warning" />&nbsp;&nbsp;&nbsp;
						<a href="?m=supplier" class="btn btn-large btn-danger"><i class="fa fa-times"></i> List</a></td>
					</tr>
                </tbody>
              </table>
			 </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
