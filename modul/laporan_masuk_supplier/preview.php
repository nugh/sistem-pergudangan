<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator Inventory Barang
      </h1>
    </section>
    <!-- Main content -->
  <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Lihat Daftar Barang Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form action="?m=laporan_masuk_supplier&s=tampilkan" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-4">
            <div class="form-group">
                <label>Pilih Tanggal</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <?php
                  include 'lib/config.php';
                  $tanggal_ = $_GET['tanggal'];
                  $tanggal = format_back_date($tanggal_);
                  $id_supplier = $_GET['id_supplier']
                  ?>
                  <input required  value="<?=$tanggal_?>" type="text" name="tanggal" class="form-control pull-right" id="datepicker1">
                </div>
                <!-- /.input group -->
              </div>
            </div>
            <div class="col-md-4">
              <label>Supplier</label>
              <div>
              <select name="id_supplier" class="form-control">
              <?php
                $sql    = "SELECT * FROM supplier ORDER BY id_supplier";
                $query  = mysqli_query($koneksi,$sql);
                while ($r=mysqli_fetch_assoc($query)){ ?>
              <option <?php if($id_supplier == $r['id_supplier']){ ?> selected="selected"<?php } ?> value="<?= $r['id_supplier'] ?>"><?= $r['nama_supplier']?></option>
                                             <?php } ?>
              </select>
            </div>

            </div>

            <div class="col-md-4">
              <input style="margin-top: 25px; " type="submit" name="simpan" value="Tampilkan" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;    
            </div>
          </div>  
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
<div class="box">
        <div class="box-body">
          <a href="index.php?m=laporan_masuk_supplier&s=export&tanggal=<?=$tanggal?>&id_supplier=<?=$id_supplier?>" 
            class="btn btn-small btn-primary" style="margin-bottom: 10px;margin-left: 90%;">Export Excel</a>

          <table id="pilkasis1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Nama Supplier</th>    
                  <th>qty</th>           
					<th>Harga Satuan</th>	
                  <th>Tanggal</th>
                  <th>Total Harga</th>
                </tr>
                </thead>
                <tbody>
                
<?php
$sql="SELECT a.*,b.nama_barang, c.nama_supplier FROM barang_masuk a
JOIN barang b ON b.kode_barang = a.kode_barang
JOIN supplier c ON c.id_supplier = a.id_supplier 
where tanggal = '$tanggal' AND a.id_supplier = '$id_supplier' ";
$query=mysqli_query($koneksi,$sql);
  $no=1;
  while($r=mysqli_fetch_assoc($query)){
    echo "<tr>";
    echo "<td>$no</td>";
    echo "<td>".$r['kode_barang']."</a></td>";
    echo "<td>".$r['nama_barang']."</a></td>";
    echo "<td>".$r['nama_supplier']."</a></td>";
    echo "<td>".$r['qty']."</td>"; 
	echo "<td>".$r['harga']."</td>";	
    echo "<td>".$r['tanggal']."</td>";  
    echo "<td>".$r['total_harga']."</td>";  
    echo "</tr>";
    $no++;
  }
?>
                </tbody>  
              </table>
          
        
        </div>  
        </div>


    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>