<?php include "atas.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator Inventory Barang
      </h1>
    </section>
    <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Lihat Daftar Barang Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form action="?m=laporan_masuk_supplier&s=tampilkan" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-4">
            <div class="form-group">
                <label>Pilih Tanggal</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input required  value="<?php echo date("m/d/Y");?>" type="text" name="tanggal" class="form-control pull-right" id="datepicker1">
                </div>
                <!-- /.input group -->
              </div>
            </div>
            <div class="col-md-4">
            <label>Supplier</label>
            <div>
              <select name="id_supplier" class="form-control">
                <?php
                include "lib/config.php";
                $sql    = "SELECT * FROM supplier ORDER BY id_supplier";
                $query  = mysqli_query($koneksi,$sql);
                while ($r=mysqli_fetch_assoc($query)) {
                  echo "<option value='".$r['id_supplier']."'>".$r['nama_supplier']."</option>";
                }
                ?>
              </select>
            </div>
          
            </div>
            <div class="col-md-4">
              <input style="margin-top: 25px; " type="submit" name="simpan" value="Tampilkan" class="btn btn-large btn-primary" />&nbsp;&nbsp;&nbsp;    
            </div>
          </div>  
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include "bawah.php"; ?>
