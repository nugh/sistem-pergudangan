<?php
$tanggal = $_GET['tanggal'];
$id_supplier = $_GET['id_supplier'];

include 'lib/fungsi.php';
include 'lib/config.php';

header("Content-Type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporanBarangMasuk.xls");

echo "<table border='1'>";
echo "<tr>";
echo "<th>No</th>";
echo "<th>Kode Barang</th>";
echo "<th>Nama Barang</th>";
echo "<th>Nama supplier</th>";
echo "<th>Qty</th>";
echo "<th>Tanggal</th>";
echo "<th>Harga</th>";
echo "<th>Total harga</th>";
echo "</tr>";

$sql="SELECT a.*,b.nama_barang, c.nama_supplier FROM barang_masuk a
JOIN barang b ON b.kode_barang = a.kode_barang
JOIN supplier c ON c.id_supplier = a.id_supplier 
where tanggal = '$tanggal' and a.id_supplier = '$id_supplier'";
$query=mysqli_query($koneksi,$sql);
$no = 1;
while ($d= mysqli_fetch_assoc($query)) {
	
	echo '

		<tr>
			<td>'.$no.'</td>
			<td>'.$d['kode_barang'].'</td>
			<td>'.$d['nama_barang'].'</td>
			<td>'.$d['nama_supplier'].'</td>
			<td>'.$d['qty'].'</td>
			<td>'.$d['tanggal'].'</td>
			<td>'.$d['harga'].'</td>
			<td>'.$d['total_harga'].'</td>
		</tr>

	';
	$no++;
}
echo "</table>";
?>