-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2019 at 11:18 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `stok`, `harga`) VALUES
('B011', 'Susu Diamond', 147, '20000'),
('B0111', 'Susu ASDS', 111, '1111111'),
('B0112', 'Susu ASD', 100, '200000'),
('B09', 'sosis', 2, '10000'),
('B2', 'Buku', 4, '9000');

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barang_keluar` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `harga` varchar(200) NOT NULL,
  `total_harga` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barang_keluar`, `kode_barang`, `qty`, `tanggal`, `harga`, `total_harga`) VALUES
(4, 'B1', 2, '2018-04-10', '90000', ''),
(5, 'B1', 2, '2018-04-10', '9000', '18000'),
(6, 'B02', 2, '2018-04-11', '20000', '40000'),
(7, 'B2', 10, '2018-04-11', '3000', '30000'),
(8, 'B1', 1, '2018-04-11', '18000', '18000'),
(9, 'B1', 1, '2018-04-11', '1000', '1000'),
(10, 'B2', 2, '2018-04-11', '8000', '16000'),
(11, 'B2', 1, '2018-04-11', '8000', '8000'),
(12, 'B2', 1, '2018-04-11', '8000', '8000'),
(13, 'B2', 1, '2018-04-11', '10000', '10000'),
(14, 'B2', 1, '2018-04-11', '20000', '20000'),
(15, 'B2', 2, '2018-04-11', '20000', '40000'),
(16, 'B09', 8, '2019-11-11', '15000', '120000'),
(17, 'B011', 3, '2019-12-01', '20000', '60000');

--
-- Triggers `barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `update_stok_keluar` AFTER INSERT ON `barang_keluar` FOR EACH ROW BEGIN
	UPDATE barang SET stok=stok - NEW.qty
	WHERE kode_barang = NEW.kode_barang;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barang_masuk` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `id_kategory` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `total_harga` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barang_masuk`, `kode_barang`, `id_kategory`, `qty`, `tanggal`, `id_supplier`, `harga`, `total_harga`) VALUES
(19, 'B1', '', 2, '2018-04-10', 4, '1000', '4000'),
(20, 'B1', '', 2, '2018-04-10', 3, '9000', '18000'),
(21, 'B1', '', 2, '2018-04-10', 3, '9000', '18000'),
(22, 'B02', '', 3, '2018-04-11', 4, '3000', '9000'),
(23, 'B2', '', 5, '2018-04-11', 3, '5000', '25000'),
(24, 'B09', '', 10, '2019-11-11', 4, '10000', '100000'),
(25, 'B2', '', 3, '2019-11-13', 7, '3000', '9000'),
(26, 'B011', '', 50, '2019-11-14', 3, '99999999', '4999999950');

--
-- Triggers `barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `update_stok_masuk` AFTER INSERT ON `barang_masuk` FOR EACH ROW BEGIN
	UPDATE barang SET stok=stok + NEW.qty
	WHERE kode_barang = NEW.kode_barang;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kategory`
--

CREATE TABLE `kategory` (
  `id_kategory` int(11) NOT NULL,
  `nama_kategory` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategory`
--

INSERT INTO `kategory` (`id_kategory`, `nama_kategory`) VALUES
(3, 'Ward'),
(12, 'sujonoi'),
(111, '11111'),
(300, 'Wardo'),
(212121, '212121');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jabatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `password`, `nama`, `jabatan`) VALUES
('', 'd41d8cd98f00b204e9800998ecf8427e', '', ' '),
('admin', '21232f297a57a5a743894a0e4a801fc3', 'ward', 'admin'),
('admin2', '21232f297a57a5a743894a0e4a801fc3', 'admin2', ' '),
('anu', '89a4b5bd7d02ad1e342c960e6a98365c', 'anu', ' '),
('wahyu', 'pass', 'nama', '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(200) NOT NULL,
  `no_telp` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `no_telp`, `alamat`) VALUES
(3, 'Nama', 'kwkww', 'kkwk'),
(4, 'Supri', '0987990', 'wkwk'),
(7, 'DinoMall', '0899', 'Sidoarjo'),
(8, 'DinoMall2', '0867', 'sda'),
(9, 'DinoMall3', '0988', 'rumah Tok Abah'),
(10, 'DinoMall5', '0867777', 'rumah Tok Abah 4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barang_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barang_masuk`);

--
-- Indexes for table `kategory`
--
ALTER TABLE `kategory`
  ADD PRIMARY KEY (`id_kategory`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
