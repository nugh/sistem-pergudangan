/*
SQLyog Professional v12.4.1 (64 bit)
MySQL - 10.1.13-MariaDB : Database - db_inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_inventory` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_inventory`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kode_barang` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` varchar(200) NOT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang` */

insert  into `barang`(`kode_barang`,`nama_barang`,`stok`,`harga`) values 
('B02','barang2',1,'2000'),
('B09','sosis',2,'10000'),
('B1','Barang1',0,'20000'),
('B2','Buku',1,'9000');

/*Table structure for table `barang_keluar` */

DROP TABLE IF EXISTS `barang_keluar`;

CREATE TABLE `barang_keluar` (
  `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `total_harga` varchar(200) NOT NULL,
  PRIMARY KEY (`id_barang_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `barang_keluar` */

insert  into `barang_keluar`(`id_barang_keluar`,`kode_barang`,`qty`,`tanggal`,`id_customer`,`harga`,`total_harga`) values 
(4,'B1',2,'2018-04-10',1,'90000',''),
(5,'B1',2,'2018-04-10',3,'9000','18000'),
(6,'B02',2,'2018-04-11',1,'20000','40000'),
(7,'B2',10,'2018-04-11',1,'3000','30000'),
(8,'B1',1,'2018-04-11',1,'18000','18000'),
(9,'B1',1,'2018-04-11',1,'1000','1000'),
(10,'B2',2,'2018-04-11',1,'8000','16000'),
(11,'B2',1,'2018-04-11',1,'8000','8000'),
(12,'B2',1,'2018-04-11',1,'8000','8000'),
(13,'B2',1,'2018-04-11',1,'10000','10000'),
(14,'B2',1,'2018-04-11',1,'20000','20000'),
(15,'B2',2,'2018-04-11',1,'20000','40000'),
(16,'B09',8,'2019-11-11',4,'15000','120000');

/*Table structure for table `barang_masuk` */

DROP TABLE IF EXISTS `barang_masuk`;

CREATE TABLE `barang_masuk` (
  `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `total_harga` varchar(200) NOT NULL,
  PRIMARY KEY (`id_barang_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `barang_masuk` */

insert  into `barang_masuk`(`id_barang_masuk`,`kode_barang`,`qty`,`tanggal`,`id_supplier`,`harga`,`total_harga`) values 
(19,'B1',2,'2018-04-10',4,'1000','4000'),
(20,'B1',2,'2018-04-10',3,'9000','18000'),
(21,'B1',2,'2018-04-10',3,'9000','18000'),
(22,'B02',3,'2018-04-11',4,'3000','9000'),
(23,'B2',5,'2018-04-11',3,'5000','25000'),
(24,'B09',10,'2019-11-11',4,'10000','100000');

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama_customer` varchar(200) NOT NULL,
  `no_telp` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

insert  into `customer`(`id_customer`,`nama_customer`,`no_telp`,`alamat`) values 
(1,'sujono','9997','hahajh'),
(3,'Ward','089789','qww'),
(4,'Cust1','890','jl');

/*Table structure for table `pengguna` */

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jabatan` varchar(200) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengguna` */

insert  into `pengguna`(`username`,`password`,`nama`,`jabatan`) values 
('admin','21232f297a57a5a743894a0e4a801fc3','ward','admin'),
('admin2','21232f297a57a5a743894a0e4a801fc3','admin2',' '),
('wahyu','pass','nama','');

/*Table structure for table `supplier` */

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(200) NOT NULL,
  `no_telp` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  PRIMARY KEY (`id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `supplier` */

insert  into `supplier`(`id_supplier`,`nama_supplier`,`no_telp`,`alamat`) values 
(3,'Nama','kwkww','kkwk'),
(4,'Supri','0987990','wkwk');

/* Trigger structure for table `barang_keluar` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `update_stok_keluar` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `update_stok_keluar` AFTER INSERT ON `barang_keluar` FOR EACH ROW BEGIN
	UPDATE barang SET stok=stok - NEW.qty
	WHERE kode_barang = NEW.kode_barang;
    END */$$


DELIMITER ;

/* Trigger structure for table `barang_masuk` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `update_stok_masuk` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `update_stok_masuk` AFTER INSERT ON `barang_masuk` FOR EACH ROW BEGIN
	UPDATE barang SET stok=stok + NEW.qty
	WHERE kode_barang = NEW.kode_barang;
    END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
